/**
 * Created by Edmund on 4/10/2018.
 */
const express = require("express");
const app = express();
const bodyParser = require('body-parser');
const sgMail = require('@sendgrid/mail');
const port = process.env.PORT || 8080;
const formEmailTo = process.env.FORM_EMAIL || 'lutemaster@gmail.com';
const allowedCORSDomain = process.env.ALLOWED_CORS_DOMAIN;
const allowedNonHTTPSDomain = allowedCORSDomain.replace("https://", "http://");

sgMail.setApiKey(process.env.SENDGRID_KEY || '');

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

app.use((req, res, next) => {
    if(req.headers.origin !== allowedCORSDomain && req.headers.origin !== allowedNonHTTPSDomain) {
        res.status(401).send(`Nice try, ${req.headers.origin}`).end();
    } else {
        next();
    }
});

const corsm = (req, res, next) => {

    const allowDomain = req.headers.origin === allowedNonHTTPSDomain ? allowedNonHTTPSDomain : allowedCORSDomain;

    res.header('Access-Control-Allow-Origin', allowDomain);
    res.header('Access-Control-Allow-Methods', 'GET, POST');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
    next();
};

app.use(corsm);

app.get("/", (req, res) => {
   res.status(200).send("HELLO WORLD");
});

app.post("/danimalwelfare", (req, res) => {
    try {
        const msg = {
            to: formEmailTo,
            from: req.body.email,
            subject: `Contacted by ${req.body.name}`,
            text: `You were contacted by ${req.body.name} from the website, they said "${req.body.details}"`,
            html: `You were contacted by 
                <strong>${req.body.name}</strong> from the website, 
                they said <br/> <i>"${req.body.details}"</i>`,
        };

        sgMail.send(msg);
        res.status(200).send("SENT EMAIL");
    } catch(ex) {
        res.status(500).send(ex);
    }

});

app.listen(port, () => {
    console.log(`The audience is listening on port: ${port}`);
});
